﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealBar : MonoBehaviour
{
    public Slider slider;
    public void SetMaxHealth(int health)
    {
        slider.maxValue = health;
        slider.value = health;
    }

    internal void SetMaxHealth(object playerHp)
    {
        throw new NotImplementedException();
    }

    public void SetHealth(int health)
    {
        slider.value = health;
    }
}
