﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healing : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;
    [SerializeField] private int healPoint;
    private float speed;
    private float augularSpeed;
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        Rotation();
    }
    private void Rotation()
    {
        speed = rb.velocity.magnitude;
        augularSpeed = rb.angularVelocity.magnitude * Mathf.Rad2Deg;
        rb.angularVelocity = new Vector3(0, Mathf.PI * 2, 0);
    }
    public void OnTriggerEnter(Collider other)
    {
        var target = other.gameObject.GetComponent<IHealingable>();
        target?.TakeHeal(healPoint);
        Destroy(gameObject);
    }
}
