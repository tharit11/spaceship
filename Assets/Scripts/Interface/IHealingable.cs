﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHealingable
{
    void TakeHeal(int heal);
}
